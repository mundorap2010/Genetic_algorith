package utils

import (
	"algoritmosGeneticos/src/model"
	"encoding/csv"
	"fmt"
	"log"
	"os"
)

func WriteCSV(data *[]model.Individual)  {
	file, err := os.Create("result.csv")
	checkError("Cannot create file", err)
	defer file.Close()

	writer := csv.NewWriter(file)
	defer writer.Flush()

	writer.Write([]string{"Binary", "Decimal", "Real", "Adapted", "Proportional"})
	for _, value := range *data {
		info := []string{format("%s", value.Binary), format("%d", value.Decimal), format("%b", value.Adapted), format("%b", value.Proportional)}
		err := writer.Write(info)
		checkError("Cannot write to file", err)
	}
}

func checkError(message string, err error) {
	if err != nil {
		log.Fatal(message, err)
	}
}

func format(value string, t interface{}) string  {
	return fmt.Sprintf(value, t)
}