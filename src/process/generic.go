package process

import . "algoritmosGeneticos/src/model"

func NewGeneric(ind *[]Individual, des *Description) *GenericDAO {
	var obj GenericDAO
	obj.Individual = ind
	obj.Description = des
	return &obj
}

type GenericDAO struct {
	Individual *[]Individual
	Description *Description
}