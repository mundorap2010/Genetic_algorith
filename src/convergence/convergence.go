package convergence

import (
	"algoritmosGeneticos/src/elitist"
	. "algoritmosGeneticos/src/model"
	"algoritmosGeneticos/src/process"
	"algoritmosGeneticos/src/roulette"
	"algoritmosGeneticos/src/tournament"
	"algoritmosGeneticos/src/utils"
	"fmt"
	"math/rand"
)

type Convergence struct {
	*GenericDAO
}

func NewConvergence(des *Description) *Convergence {
	return &Convergence{NewGeneric(des)}
}

func (convergence *Convergence) getMaximum(lengthData, convergenceValue int) int {
	return (convergenceValue * lengthData) / 100
}

func (convergence *Convergence) mutation(data *[]Individual, total int) {
	for i := 0; i <= total; i++ {
		position := rand.Intn(len(*data)-0) + 0
		item := (*data)[position]
		byteValue := rand.Intn(len(item.Binary)-0) + 0
		out := []rune(item.Binary)
		if out[byteValue] == '0' {
			out[byteValue] = '1'
		} else {
			out[byteValue] = '0'
		}
		individual := utils.ReturnIndividual(string(out), len(string(out)), convergence.Description.XMin, convergence.Description.XMax)
		var individuals []Individual
		individuals = append(individuals, individual)
		utils.GetProportion(&individuals, utils.GetProportionValue(data))
		(*data)[position] = individuals[0]
	}
}

func (convergence *Convergence) getDataWork() *[]Individual {
	//dataWork := []string{"01011101", "10110101", "10010100", "10000111", "01101110", "10011110"}
	dataWork := utils.GenerateData(int(convergence.Description.Length), convergence.Description.Total)
	valueLength := len((*dataWork)[0])
	var individuals []Individual
	for _, element := range *dataWork {
		individuals = append(individuals, utils.ReturnIndividual(element, valueLength, convergence.Description.XMin, convergence.Description.XMax))
	}
	return &individuals
}

func (convergence *Convergence) getDataWorkTournament() *[]Individual {
	return tournament.Order(8, convergence.Description.XMin, convergence.Description.XMax, 20)
}

func (convergence *Convergence) returnData()  *[]Individual {
	var individuals *[]Individual
	if convergence.Description.Tournament == false {
		individuals = convergence.getDataWork()
	} else {
		individuals = convergence.getDataWorkTournament()
	}
	return individuals
}

func (convergence *Convergence) Do() {
	var finalData []Individual
	var individuals = convergence.returnData()
	var strong = &Individual{}
	var strong_temp = &Individual{}
	utils.GetProportion(individuals, utils.GetProportionValue(individuals))
	utils.DrawTable(individuals)
	fmt.Println(int(utils.GetAdapted(float64(convergence.Description.XMax))))
	fmt.Println(convergence.getMaximum(len(*individuals), convergence.Description.Convergence))
	for len(finalData) < convergence.getMaximum(len(*individuals), convergence.Description.Convergence) {
		if convergence.Description.Tournament == false {
			roulette.Order(individuals)
		} else {
			individuals = convergence.returnData()
		}
		fmt.Println("******** Roulette ********")
		utils.DrawRoulette(individuals)
		// Init general process
		process.NewProcess(individuals, convergence.Description).Init()
		fmt.Println("******** Mutation ********")
		convergence.mutation(individuals, 1)
		utils.DrawRoulette(individuals)
		// Check if any value already has max value
		for _, element := range *individuals {
			if element.Adapted > utils.GetAdapted(float64(convergence.Description.XMax)) {
				finalData = append(finalData, element)
			}
		}
		if (&Individual{}) != strong {
			strong = elitist.Elitist(individuals)
		} else {
			strong_temp = elitist.Elitist(individuals)
		}
		if strong_temp.Adapted > strong.Adapted {
			individuals = toSlice(*individuals, *strong)
			strong = strong_temp
		}

	}
	fmt.Println("******** Final ********")
	utils.DrawRoulette(&finalData)
	utils.WriteCSV(&finalData)
}

func toSlice(data []Individual, strong Individual) *[]Individual {
	data = append(data, strong)
	return &data
}
