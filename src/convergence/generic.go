package convergence

import . "algoritmosGeneticos/src/model"

func NewGeneric(des *Description) *GenericDAO {
	var obj GenericDAO
	obj.Description = des
	return &obj
}

type GenericDAO struct {
	Description *Description
}
