package roulette

import (
	"algoritmosGeneticos/src/model"
	"github.com/pmylund/sortutil"
)

func Order(data *[]model.Individual)  {
	sortutil.DescByField(*data, "Adapted")
}